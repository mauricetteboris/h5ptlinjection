# h5pTLinjection

## Description

Petit outil d'injections de données dans une timeline h5p: pour les
enseignants, dans l'activité moodle/Éléa, l'interface permet de saisir des
données une à une: c'est fastidieux.

Dans la version python, un petit module permet d'injecter directement des données 

Usage:

```
python -m h5pTLinjection
```
des fenêtres permettent de sélectionner le fichier h5p, les données (csv ou json) puis un dossier de sortie.

Dans la version javascript, tout se passe dans votre navigateur: quelques lignes de code et deux modules (papaparse, JSZip) font le reste.

La page est déployée sur la partie publique de ce projet: [page publique](https://mauricetteboris.forge.apps.education.fr/h5ptlinjection)

## Installation du module python

Le module est déployé sur le gestionnaire de paquets de la forge, il suffit donc de taper en ligne de commande:

```
pip install h5pTLinjection --index-url https://forge.apps.education.fr/api/v4/projects/1762/packages/pypi/simple
```

## Support

Visitez le [Wiki](https://forge.apps.education.fr/mauricetteboris/h5ptlinjection/-/wikis/home) du projet pour voir des exemples d'usage.

Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap

* ajouter des tests unitaires pour js et jobs
* accepter plus de champs


