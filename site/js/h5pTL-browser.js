// var fs = require("fs");
// var fsP = require('node:fs/promises');// require("fs/promises");
// var JSZip = require("jszip");
// var Papa = require("papaparse");

// surtout pas de flag "g"
const h5pdate =  new RegExp("(?<s>-)?(?<y>[0-9]{1,4})(,(?<m>[0-9]{1,2}),(?<d>[0-9]{1,2}))?");
const negdate = new RegExp("(?<s>-)?(?<a>.*)$");


class Validator{
    date(S){
	if (S=="")
	    return ""
	var d = h5pdate.exec(S);
	if (d  && S.includes(",")){
	    //console.log(d);
	    var signe = (!(d.groups.s === undefined) ? d.groups.s : ""); 
	    if (S.length <= 5)
		return signe + d.groups.y.toString().padStart(4, '0')
	    else if (d.groups.m && d.groups.d){
		let y = d.groups.y.toString().padStart(4, '0');
		let m = d.groups.m.toString().padStart(2, '0');
		let day = d.groups.d.toString().padStart(2, '0');		
		// il faut peut-être créer un new Date pour gérer les exceptions
		return `${signe}${y},${m},${day}`
	    }
	}
	else {
	    d = negdate.exec(S).groups;
	    signe = (!(d.s === undefined) ? d.s : ""); 
	    let D = new Date(d.a);
	    let y = D.getFullYear().toString().padStart(4, '0');
	    let m = D.getMonth().toString().padStart(2, '0');
	    let day = D.getUTCDate().toString().padStart(2, '0');
	    return `${signe}${y},${m},${day}`;
	}
    }

    champs_ok(o){
	let keys = Object.keys(o);
	return (keys.includes("startDate") && keys.includes("headline"));
    }

    to_h5p(o){
	var out = {};
	var keys = Object.keys(o);
	if (this.champs_ok(o) == true){
	    ["startDate", "endDate"].forEach( (e) => { if (keys.includes(e)){
		out[e] = this.date(o[e])} });
	    ["headline", "text", "tag"].forEach( (e) => { if (keys.includes(e)){
		out[e] = o[e]} });
	    out["asset"] = {};
	    ["media", "credit", "caption"].forEach( (e) => { if (keys.includes(e)){
		out["asset"][e] = o[e]} });
	    return out
	}
	else { return null }
    }

}

class h5pInjector{
    v = new Validator();
    
    consume(data){
	var DATA = [];
	var ERR = [];
	var d;
	data.forEach((e,i)=> {
	    try {
		d = this.v.to_h5p(e);
		if (d) { DATA.push(d) }
	    } catch (error) {
		e['error'] = `${i}: entrée rejetée - ${error}`; 
		ERR.push(e);
	    }
	});
	return {'data': DATA, 'errors': ERR}
    };
    /* correspond à la version nodejs
    inject(h5p, data){
	var nom = h5p.substring(0, h5p.length - 4);
	var ct;
	var D;

	let P1 = [];
	let P2;
	Papa.parse(data, {download: true,
			  header: true,
			  delimiter: ';',
			  skipEmptyLines: true,
			  complete: function(results, file) {
			      P1 = results.data;
			      console.log("Parsing complete:", P1);
			  }})
    };
    */
}

const INJ = new h5pInjector();
const H5P = document.getElementById('h5p');
const DATES= document.getElementById('data');
const ERA= document.getElementById('era');
const s = document.getElementById('submit');
s.addEventListener('click', ()=>{
    injection(DATES, ERA)
} )

function injection(f, g){
    let P1 = [];
    if (f.files.length == 1) {	
	if (f.files[0].endsWith("sv")){ // cas des .csv, .tsv
	    Papa.parse(f.files[0],
		       {download: true,
			header: true,
			delimiter: '', // auto-detect? ';'
			skipEmptyLines: true,
			complete: function(results, file) {
			    P1 = results.data;
			    console.log("Parsing complete - événements:", P1);
			}})}
	// cas des .json désactivé
    }	
    let P2 = [];
    if (g.files.length == 1) {
	if (g.files[0].endsWith("sv")){ // cas des .csv, .tsv
	    Papa.parse(g.files[0],
	       {download: true,
		header: true,
		delimiter: '', // auto-detect? ';'
		skipEmptyLines: true,
		complete: function(results, file) {
		    P2 = results.data;
		    console.log("Parsing complete - périodes:", P2);
		}})}
	// cas des .json désactivé
    }
        
    let P3 = JSZip.loadAsync(H5P.files[0])
	.then(function (zip) {
 	    return zip.file("content/content.json").async("text")
	})
	.then( (text) => JSON.parse(text))
	.then( (content) => {
	    let D = INJ.consume(P1);
	    let DE = INJ.consume(P2);
	    console.log(D, DE);

	    let K = Object.keys(content.timeline);
	    // màj clef "date"
	    if (K.includes("date")) { content.timeline.date = content.timeline.date.concat(D.data);}
	    else { content.timeline.date = D.data}
	    // màj clef "era"
	    if (K.includes("era")) { content.timeline.era = content.timeline.date.concat(DE.data);}
	    else { content.timeline.era = DE.data}

	    //content.timeline.date = content.timeline.date.concat(D.data);

	    let outlook = new RegExp("(.*).h5p");
	    let out = outlook.exec(H5P.files[0].name); // extraction nom sans suffixe
	    JSZip.loadAsync(H5P.files[0])
 		.then(function (zip) {
		    zip.remove('content/content.json')
		    zip.file('content/content.json', JSON.stringify(content),
			     {createFolders: false}) // importante option
		    zip.generateAsync({type:"base64"}).then(function (base64) {
			var location = document.getElementById('dld');
			location.href = "data:application/zip;base64," + base64;
			location.download = `${out[1]}i.h5p`;
			location.setAttribute("class", "ready");
			var text = document.createTextNode("Cliquer pour télécharger");
			location.replaceChildren(text);

			var infos = document.getElementById('inject');
			var b1 =  document.createTextNode(`#entrées injectées évenements/périodes (${D.data.length}/${DE.data.length}) - rejets (${D.errors.length}/${DE.errors.length}):`);
			infos.replaceChildren(b1);

			var err = document.getElementById('errors');
			D.errors.forEach((e) => {
			    let a = document.createElement("li", e.tostring());
			    err.appendChild(a)
			})
			DE.errors.forEach((e) => {
			    let a = document.createElement("li", e.tostring());
			    err.appendChild(a)
			})
		    })
 			.catch(error => console.error(error));
 		    // // sauvegarder via saveAs de FileSaver.js; 
		    // zip.generateAsync({type: 'blob', mimeType: 'application/zip',
		    // 		   comment: 'h5pTL injection'})
		    //     .then(function (blob) {
		    // 	saveAs(blob, "essai.h5p");
		    //     })
		})
	})
	.catch(error => console.error(error));
}
