import unittest
from datetime import datetime
from src.h5pTLinjection.h5pTL import Validator, h5pInjector

class TestObj(unittest.TestCase):
    def setUp(self):
        self.v = Validator()
        self.i = h5pInjector()
        self.o1 = {"a": 1, "headline": "titre"}
        self.o2 = {"a": 1, "headline": "titre", "startDate": "bad2024,08,36"}
        self.d3 = "1882-03-23T00:00:00Z" # "naissance Emmy Noether"

        
    def test_date_simple(self):
        self.assertEqual(self.v.date("2024,05,06"), "2024,05,06",
                         "mauvais formatage simple")

    def test_date_negative1(self):
        self.assertEqual(self.v.date("-540"), "-540",
                         "date négative")

    def test_date_1car(self):
        self.assertEqual(self.v.date("2024,5,6"), "2024,05,06",
                         "mauvais formatage 1 seul caractère mois ou jour")
        
    def test_date_negative2(self):
        self.assertEqual(self.v.date("-2024,5,6"), "-2024,05,06",
                         "date négative 3 champs")
        
    def test_date_moistropgrand(self):
        self.assertRaises(ValueError, self.v.date, "2024,13,6")

    def test_date_jourtropgrand(self):
        self.assertRaises(ValueError, self.v.date, "2024,08,36")

    def test_date_ISO1(self):
        self.assertEqual(self.v.date("2024-08-25T12:45:07"), "2024,08,25")

    def test_date_ISO2(self):
        self.assertEqual(self.v.date("2024-08-25"), "2024,08,25")

    def test_date_ISO3_Zsuffix(self):
        self.assertEqual(self.v.date(self.d3), "1882,03,23")

    def test_date_negative_ISO2(self):
        self.assertEqual(self.v.date("-2024-05-06"), "-2024,05,06",
                         "date négative 3 champs")
    def test_date_negative_ISO1(self):
        self.assertEqual(self.v.date("-2024-08-25T12:45:07"), "-2024,08,25")
        
    def test_champ1(self):
        # manque présence d'une startDate
        self.assertFalse(self.v.champs_ok(self.o1))

    def test_champ2(self):
        # les 2 champs y sont mais mauvais format de startDate
        self.assertTrue(self.v.champs_ok(self.o2))

    def test_data1(self):
        D = {"headline": "titre1", "startDate": "2024", "text": "quelquechose"}
        Dout = D.copy()
        Dout["asset"] = {}
        self.assertEqual(self.i.consume([D]), {"data": [Dout], "erreurs": []}, "consommation d'une donnée simple")

    def test_data2(self):
        D = {"headline": "titre1", "startDate": "2024", "text": "quelquechose", "media":"https://upload.wikimedia.org/wikipedia/commons/thumb/8/82/Georg_Friedrich_Bernhard_Riemann.jpeg/330px-Georg_Friedrich_Bernhard_Riemann.jpeg", "caption": "Bernhard Reimann", "autre": "rien"}
        Dout = D.copy()
        del Dout["autre"], Dout["media"], Dout["caption"]
        Dout["asset"] = {"media":"https://upload.wikimedia.org/wikipedia/commons/thumb/8/82/Georg_Friedrich_Bernhard_Riemann.jpeg/330px-Georg_Friedrich_Bernhard_Riemann.jpeg", "caption": "Bernhard Reimann"}
        self.assertEqual(self.i.consume([D]), {"data": [Dout], "erreurs": []}, "consommation d'une donnée avec asset")

    def test_data_corrupted1(self):
        self.assertEqual(self.i.consume([self.o2]), {"data": [], "erreurs": ["0: entrée rejetée: titre"]})

if __name__ == '__main__':
    unittest.main()
