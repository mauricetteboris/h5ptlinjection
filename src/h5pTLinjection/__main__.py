from tkinter import Tk, Frame, W
from tkinter import ttk
from tkinter.filedialog import askopenfilename, askdirectory
from os.path import join as opjoin
from os.path import dirname
from .h5pTL import h5pInjector


class h5pInjApp(Tk):
    def __init__(self):
        super().__init__()

        self.geometry("700x250")
        self.title("h5pInjection")
        self.maxsize(900, 400)

        self.columnconfigure(0, weight=1)
        self.columnconfigure(1, weight=4)
        
        src_button = ttk.Button(self, text="Source h5p", command=self.select_src, width=20).grid(column=0, row=0)
        self.label_src = ttk.Label(self, text="")
        self.label_src.grid(column=1, row=0)
        date_button = ttk.Button(self, text="Fichier dates", command=self.select_date, width=20).grid(column=0, row=1)
        self.label_date = ttk.Label(self, text="")
        self.label_date.grid(column=1, row=1)
        era_button = ttk.Button(self, text="Fichier périodes", command=self.select_era, width=20).grid(column=0, row=2)
        self.label_era = ttk.Label(self, text="")
        self.label_era.grid(column=1, row=2)
        out_button = ttk.Button(self, text="Dossier sortie", command=self.select_receive, width=20).grid(column=0, row=3)
        self.label_out = ttk.Label(self, text="")
        self.label_out.grid(column=1, row=3)
        ttk.Button(self, text="Injecter!", command=self.appInj, width=20).grid(column=0, row=5)
        for widget in self.winfo_children():
            widget.grid(padx=5, pady=5, sticky=W)

    def select_src(self):
        self.src_file = askopenfilename(parent=self, title="Sélectionner le Fichier h5p Timeline",
                                   filetypes=[("h5p", "*.h5p")])
        self.label_src.configure(text=(self.src_file if isinstance(self.src_file, str) else "--"))

    def select_date(self):
        self.date_file = askopenfilename(parent=self, title="Sélectionner le Fichier des données csv/tsv/json",
                                    filetypes=[("csv", "*.csv"), ("tsv", "*.tsv"), ("json", "*.json")])
        self.label_date.configure(text=(self.date_file if isinstance(self.date_file, str) else "--"))

    def select_era(self):
        self.era_file = askopenfilename(parent=self, title="Sélectionner le Fichier des périodes/ères csv/tsv/json",
                                   filetypes=[("csv", "*.csv"), ("tsv", "*.tsv"), ("json", "*.json")])
        self.label_era.configure(text=(self.era_file if isinstance(self.era_file, str) else "--"))

    def select_receive(self):
        data_dir = (dirname(self.date_file) if isinstance(self.date_file, str) else "")
        self.receivePath = askdirectory(parent=self, title="Dossier de destination",
                                        initialdir=data_dir)
        self.label_out.configure(text=(self.receivePath if isinstance(self.receivePath, str) else "--"))

    def appInj(self):
        INJ = h5pInjector()
        INJ.inject(self.src_file, dates=self.date_file, era=self.era_file, outdir=self.receivePath)
        

if __name__ == "__main__":
    bApp = h5pInjApp()
    bApp.mainloop()
