import zipfile as zf
import json
import re
import csv
from datetime import datetime
from dateutil.parser import isoparse
from rdflib.query import Result
from os.path import basename, dirname, exists
from os.path import join as opjoin

class Validator():
    """objet de validation d'une entrée `dict`"""

    h5pdate = re.compile("^(-)?([0-9]{1,4})(,([0-9]{1,2}),([0-9]{1,2}))?")
    negdate = re.compile("^(-)?(.*)$")
    
    def date(self, s):
        """renvoi la date de `s` au format correct supporté par h5p:
        AAAA,MM,DD ou AAAA
        ces formats sont valides, ainsi que l'ISO 8601
        dates négatives B.C. à clarifier: < datetime.MINYEAR

        une exception ValueError est renvoyée par datetime en cas de problème.
        """
        if s == "":
            return ""
        d = self.h5pdate.search(s)
        if d:
            # gestion du signe
            signe = ("" if not(d.group(1)) else d.group(1))
            
            if len(s)<=5:
                D = datetime(int(d.group(2)), 1, 1)
                return signe + D.strftime("%Y")
            elif d.group(4) and d.group(5):
                year, month, day = map(int, (d.group(2), d.group(4), d.group(5)))
                D = datetime(year, month, day)
                return signe + D.strftime("%Y,%m,%d")
            else:
                d = self.negdate.search(s)
        else:
            d = self.negdate.search(s)
        signe = ("" if d and not(d.group(1)) else d.group(1))
        D = isoparse(d.group(2)) # datetime.fromisoformat ne reconnaît pas le suffixe Z
        # voir library/datetime.html#datetime.datetime.fromisoformat
        return signe + D.strftime("%Y,%m,%d")

    def champs_ok(self, o):
        """vérifier la présence des champs vitaux d'un objet"""
        return ("startDate" in o) and  ("headline" in o)

    def to_h5p(self, o):
        """renvoyer un objet filtré avec les bons champs

        :type o: dict
        :rtype: dict
        """
        out = {}
        assert self.champs_ok(o) == True
        out.update({e: self.date(o[e]) for e in ["startDate", "endDate"] if e in o})
        out.update({e: o[e] for e in ["headline", "text", "tag"] if e in o})
        out["asset"] = {}
        out["asset"].update({e: o[e] for e in ["media", "credit", "caption"] if e in o})
        return out


class h5pInjector():
    """objet qui consomme des données avec champs aplatis json/csv ou 
    rdflib.query.Result et les injecte à un fichier .h5p de timeline

    texte et URI uniquement, chargement de petites images non pris en charge.
    """
    v = Validator()
    
    def consume(self, data):
        """consommer un itérateur et preparer une liste d'objets compatibles pour la 
        timeline h5p

        data est une liste de dict, un itérateur csv.DictReader ou un
        rdflib.query.Result: iterable
        """
        DATA = {"data": [], "erreurs": []}
        for i,row in enumerate(data):
            try:
                DATA["data"].append(self.v.to_h5p(row))
            except ValueError:
                DATA["erreurs"].append(f"{i}: entrée rejetée: {row['headline']}")
            except AssertionError:
                DATA["erreurs"].append(f"{i}: mauvais titre(headline) ou mauvaise date(startDate)")
            except AttributeError:
                DATA["erreurs"].append(f"{i}: sûrement un probblème de reconnaissance dans les dates")
        return DATA

        
    def inject(self, h5p:str, dates=(), era=(), outdir=None):
        """injecter les données

        dates: str de chemin vers csv/json des données ou instance de rdflib.Result
        era: str de chemin vers csv/json des périodes ou instance de rdflib.Result

        ces données doivent être itérables (ne pas regrouper tout le monde dans un même json)
        par défaut tuple vide, pour accepter le chargement direct via __main__
        """
        nom = basename(h5p).removesuffix(".h5p")
        IN = zf.ZipFile(h5p)
  
        if outdir is None and isinstance(dates, str):
            outdir = dirname(dates)
        elif outdir is None: # dates est alors un Result sparql
            outdir = dirname(h5p)
        # existence d'un fichier d'inclusion à prendre en compte
        n = 1
        while exists(opjoin(outdir, f"{nom}i{n}.h5p")):
            n += 1
        nom_out = opjoin(outdir, f"{nom}i{n}.h5p")
        with IN.open('content/content.json') as F:
            ct = json.load(F)
        ########################
        # chargement des dates #
        ########################
        if isinstance(dates, str):
            if dates.endswith("sv"): # .csv ou .tsv
                F = open(dates, encoding='utf-8')
                dialect = csv.Sniffer().sniff(F.readline(), delimiters=",;\t")
                F.seek(0)
                # la 1ere ligne donne les noms des champs du DictReader
                D = csv.DictReader(F, dialect=dialect) # quotechar='"', delimiter=';'
            elif dates.endswith(".json"):
                D = json.load(dates)
        elif isinstance(dates, Result):
            # récupérer les résultats en json!
            J = json.loads(dates.serialize(format="json"))["results"]["bindings"]
            D = map(lambda e:{ k:v["value"] for k,v in e.items()}, J)
        elif dates==():
            D = []
        DT = self.consume(D)
        if isinstance(dates, str) and (dates.endswith("sv")):
            F.close()
        ###############################
        # rebelotte pour les périodes #
        ###############################
        if isinstance(era, str):
            if era.endswith("sv"): # .csv ou .tsv
                F = open(era, encoding='utf-8')
                dialect = csv.Sniffer().sniff(F.readline(), delimiters=",;\t")
                F.seek(0)
                # la 1ere ligne donne les noms des champs du DictReader
                D = csv.DictReader(F, dialect=dialect) # quotechar='"', delimiter=';'
            elif era.endswith(".json"):
                D = json.load(era)
        elif isinstance(era, Result):
            # récupérer les résultats en json!
            J = json.loads(era.serialize(format="json"))["results"]["bindings"]
            D = map(lambda e:{ k:v["value"] for k,v in e.items()}, J)
        elif era==():
            D = []
        DE = self.consume(D)
        if isinstance(era, str) and (era.endswith("sv")):
            F.close()

        # injection finale
        if "date" in ct["timeline"]:
            ct["timeline"]["date"].extend(DT["data"])
        else:
            ct["timeline"]["date"] = DT["data"]
        if "era" in ct["timeline"]:
            ct["timeline"]["era"].extend(DE["data"])
        else:
            ct["timeline"]["era"] = DE["data"]

        print(f"# dates injectées ({len(DT['data'])})\n# périodes injectées ({len(DE['data'])})")
        print(f"dates rejetées ({len(DT['erreurs'])})", *DT["erreurs"], sep="\n")
        print(f"périodes rejetées ({len(DE['erreurs'])})", *DE["erreurs"], sep="\n")
        
        with zf.ZipFile(nom_out, mode="x") as OUT:
            for e in IN.namelist():
                if e != 'content/content.json': #recopie des fichiers sous forme de bytes
                    F = IN.read(e)
                    OUT.writestr(e, F)
                else:
                    OUT.writestr(e, json.dumps(ct, ensure_ascii=False,
                                               separators=(',', ':')))
